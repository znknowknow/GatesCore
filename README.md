# 时空之门前端代码生成器
##  开发者手册已公布
## 让代码生成器成为大家开发程序的一项优势

### 简介
时空之门前端代码生成器，是第四代动词算子式代码生成器，经过彻底的重构的先进动词算子式代码生成器，也是专用的Vue+ElementUI前端代码生成器，可以和多种后端代码生成器搭配。和平之翼和光对前端代码生成的支持是通过引用不同的时空之门的版本实现的。

本代码生成器的代码生成物基于开源软件Vue-Element-Admin,感谢原作者的贡献。至此，动词算子式代码生成器阵列中所有自制的组件都已经开源，最大限度的利于大家的学习。

这个代码生成器其实是完整代码生成器的前端代码生成组件，使用时是和和平之翼代码生成器或者第三代动词算子式代码生成器：光配合使用的。您可以下这些代码生成器使用，其中的前端项目其实是时空之门代码生成器生成的。这时，时空之门的jar包是放在宿主代码生成器的lib里的。‘

时空之门前端代码生成器支持独立的Swing界面，可以独立运行。

本软件推荐从源码编译，因为某种原因，编译好的jar无法在附件处上传。如有知者，请告，多谢。

本代码生成器的操作录屏也已经公布，请去本站附件处下载。
[https://gitee.com/jerryshensjf/GatesCore/attach_files](https://gitee.com/jerryshensjf/GatesCore/attach_files)   


### 介绍视频地址

B站地址：
[https://www.bilibili.com/video/BV1y541157D7](https://www.bilibili.com/video/BV1y541157D7)

### 工程使用

本代码生成器其实是动词算子式代码生成器阵列通用的前端代码生成解决方案，真正使用时请使用Java通用代码生成器：光生成完整的前端和配套的后端项目。故请下载光使用。
项目地址：

[https://gitee.com/jerryshensjf/LightSBMEU](https://gitee.com/jerryshensjf/LightSBMEU)


二进制发布版下载：

[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)


也可以使用同样是Swing界面的光之翼Java通用代码生成器。光之翼使用时空之门4.5.0Beta10和光2.1.0 Beta3的前后端生成引擎。

项目地址：

[https://gitee.com/jerryshensjf/LightWing](https://gitee.com/jerryshensjf/LightWing)


二进制发布版下载：

[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)

### 项目图片

#### 时空之门
![输入图片说明](https://images.gitee.com/uploads/images/2019/0610/220701_41a056e0_1203742.jpeg "wormhole.jpg")

### 动词算子的力量

向Lisp和Lambda算子致敬

愿动词算子的力量与你同在

![输入图片说明](https://images.gitee.com/uploads/images/2019/0109/211947_432567db_1203742.png "magic_v.png")

### 研发进展

已释出时空之门前端代码生成器Swing独立版 ４.5.0 Beta10版。

时空之门前端代码生成器Swing独立版 ４.5.0 Beta 10版。已可以在本站附件处下载。此版本需要装好Java，双击即可运行，是Swing界面的。下载地址：
[https://gitee.com/jerryshensjf/GatesCore/attach_files](https://gitee.com/jerryshensjf/GatesCore/attach_files)   

### 新版特性 
本版的界面和操作经过了仔细调整和测试，比较直观，不易出错。
本版的生成的前端可以支持第三的动词算子式代码生成器光2.1.0 信念的最新版本。
支持Language，下划线分隔的字段名，字段否定功能。同时也支持光2.0.0的两个重要功能群动态椰子树文件夹结构和动词否定功能群。
非常先进灵活和易用，您值得一试。

### 动词算子式代码生成器的基本设计

简单说一下，我的代码生成器有三层，项目，棱柱和域对象。有大概20个动词算子，或者称为泛型动词算子。比如说，新增就是一个动词算子，编辑是另外一个动词算子。您可以选择一个域对象上面使用的动词算子，当然要遵守一些依赖关系。还可以在域对象之间建立一对多和多对多关系。这样，一个正确设置的Excel工作簿会被编译成项目，这就是动词算子式代码生成器的基本逻辑。 

### 注意

因为眼疾，我不得不把动词算子式代码生成器的研发工作停止在目前的状态。研发工作已经延续了七年，有很多成就，也有很多遗憾，更有很多已规划但却从未实现的功能。现在，我把这些内容功能公布出来。也许，呼唤一位英雄，也许是几位。

至尊宝，这是您的箍和屠龙宝刀。接好了，屠龙刀乃是宝物，不要说砸到小朋友，砸到了花花草草也是不好的。

我将在开源中国博客中公布所有未完成的功能构想。并基于光1.5.0的代码编写《开发者手册》（《黑客手册》）

#### 开发者手册（“黑客手册”）下载地址
[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)

#### 开发者手册（“黑客手册”）截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/190512_ae13696a_1203742.png "hacker.png")

#### 相关技术博客
地址：[https://my.oschina.net/jerryshensjf](https://my.oschina.net/jerryshensjf)

#### 技术博客截图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/201120_eae9fbef_1203742.png "blog.png")

#### 时空之门独立版Excel代码生成界面截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1010/211914_90eea718_1203742.png "gatescore_4_5_b6.png")

#### 时空之门独立版SGS代码生成界面截图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1122/171348_59117bbf_1203742.png "GatesCore_Full_3_0_0_SGS.png")

### 代码生成物截图：

登录：
![登录](https://images.gitee.com/uploads/images/2019/0415/214758_8c47b686_1203742.png "vue_login.png")

Grid:
![Grid](https://images.gitee.com/uploads/images/2019/0415/214815_c2dfdd1e_1203742.png "vue_bonuses.png")

多对多：
![多对多](https://images.gitee.com/uploads/images/2019/0415/220549_b19d2ca4_1203742.png "Vue_mtm.png")

编辑，下拉列表：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0416/085420_45584d04_1203742.png "vue_update_dropdown.png")

### 软件架构
ElementUI, Vue, JSON 基于开源软件vue-element-admin的架构

### 交流QQ群
无垠式代码生成器群  277689737

