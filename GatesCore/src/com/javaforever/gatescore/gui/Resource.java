package com.javaforever.gatescore.gui;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class Resource {
	protected static Logger logger = Logger.getLogger(Resource.class);
	public List<String> getResourceStr(String fileName) throws IOException {
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(this.getClass().getResourceAsStream(fileName),"UTF-8"))) {
			List<String> contents = new ArrayList<>();
			String s = "";
			while ((s = br.readLine()) != null) {
				logger.debug(s);
				contents.add(s);
			}
			return contents;
		}
	}

	public  void downloadRes(String filePath, String newFileName) throws IOException {
		newFileName = newFileName.replace("//","/");
		logger.debug("JerryDebug:"+filePath+":"+newFileName);
		File soureFile = new File(filePath);
		File newFile = new File(newFileName);
		if (soureFile.exists()) {
			  Files.copy(soureFile.toPath(), newFile.toPath());
		}else {
			try (BufferedInputStream bi = new BufferedInputStream(this.getClass().getResourceAsStream(filePath))) {
				if (bi!=null) {
					try (BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(newFile))) {
						byte[] cache = new byte[1024];
						while (bi.read(cache) != -1) {
							bo.write(cache);
						}
					}
				}
			}
		}
	}
}