package com.javaforever.gatescore.gui;

public class AppStarter {
	public static AppGui appGui = new AppGui();
	public static ParseExcelGui peGui = new ParseExcelGui();
	
	public static void main(String [] argv) {
		switchToExcel();
	}
	public static void switchToSGS() {
		peGui.setVisible(false);
		appGui.setVisible(true);
	}
	
	public static void switchToExcel() {
		appGui.setVisible(false);
		peGui.setVisible(true);		
	}

}
