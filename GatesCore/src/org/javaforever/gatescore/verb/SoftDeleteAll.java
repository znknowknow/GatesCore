package org.javaforever.gatescore.verb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.exception.ValidateException;
import org.javaforever.gatescore.utils.StringUtil;

public class SoftDeleteAll extends FrontVerb {
	private static final long serialVersionUID = -7338969586783120609L;

	public SoftDeleteAll(FrontDomain d)  throws ValidateException{
		super(d);
		this.denied = domain.isVerbDenied("SoftDeleteAll");
		this.setVerbName("SoftDeleteAll" + d.getCapFirstPlural());
	}

	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			FrontMethod method = new FrontMethod();
			method.setStandardName("softDeleteAll" + StringUtil.capFirst(this.domain.getPlural()));
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L, 1, "softDeleteAll" + this.domain.getCapFirstPlural() + "() {"));
			sList.add(new Statement(2000L, 1, "var data = this.selectList;"));
			sList.add(new Statement(3000L, 1, "if (data.length < 1) return;"));
			sList.add(new Statement(4000L, 1,
					"softDeleteAll" + this.domain.getCapFirstPlural() + "(this.buildIds(data)).then(response => {"));
			sList.add(new Statement(5000L, 2, "this.reload();"));
			sList.add(new Statement(6000L, 1, "});"));
			sList.add(new Statement(7000L, 1, "},"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			FrontMethod method = new FrontMethod();
			method.setStandardName("softDeleteAll" + StringUtil.capFirst(this.domain.getPlural()));
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L, 0,
					"export function softDeleteAll" + this.domain.getCapFirstPlural() + "(ids) {"));
			sList.add(new Statement(2000L, 0, "return request({"));
			if (this.domain.isUseController()) {
				sList.add(new Statement(3000L, 1,
						"url: '" + this.domain.getLowerFirstDomainName() + this.domain.getControllerNamingSuffix()
								+ "/softDeleteAll" + this.domain.getCapFirstPlural() + "',"));
			} else {
				sList.add(new Statement(3000L, 1, "url: '" + this.domain.getLowerFirstDomainName()
						+ "Facade/softDeleteAll" + this.domain.getCapFirstPlural() + "',"));
			}
			sList.add(new Statement(4000L, 1, "method: 'post',"));
			sList.add(new Statement(5000L, 1, "params:{ids:ids}"));
			sList.add(new Statement(6000L, 0, "})"));
			sList.add(new Statement(7000L, 0, "}"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}
}
