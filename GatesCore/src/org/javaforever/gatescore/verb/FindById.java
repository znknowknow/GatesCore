package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.exception.ValidateException;

public class FindById extends FrontVerb{
	private static final long serialVersionUID = 423738574845411817L;

	public FindById(FrontDomain d) throws ValidateException{
		super(d);		
		this.denied = domain.isVerbDenied("FindById");
		this.setVerbName("FindById"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}
}
