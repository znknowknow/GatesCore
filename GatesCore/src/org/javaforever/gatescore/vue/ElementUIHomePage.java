package org.javaforever.gatescore.vue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.StatementList;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;

public class ElementUIHomePage extends ElementUIPage implements Comparable<ElementUIHomePage> ,Cloneable,Serializable{
	private static final long serialVersionUID = -7715930714493390663L;
	protected String stanadardName;
	protected Long serial = 0L;
	protected String language = "chinese";

	@Override
	public int compareTo(ElementUIHomePage o) {
		return this.stanadardName.compareTo(o.getStanadardName());
	}

	public String getStanadardName() {
		return stanadardName;
	}

	public void setStanadardName(String stanadardName) {
		this.stanadardName = stanadardName;
	}

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	@Override
	public StatementList generateStatementList() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<template>"));
		sList.add(new Statement(2000L,0,"<div class=\"app-container\">"));
		if (this.getLanguage().equalsIgnoreCase("english")) {
			sList.add(new Statement(3000L,0,"<h3>Welcome to the verb operator code generator world!</h3>"));
		}else {
			sList.add(new Statement(3000L,0,"<h3>欢迎来到动词算子式代码生成器世界！</h3>"));
		}
		sList.add(new Statement(4000L,0,""));
		sList.add(new Statement(5000L,0,"</div>"));
		sList.add(new Statement(6000L,0,"</template>"));
		return WriteableUtil.merge(sList);
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
