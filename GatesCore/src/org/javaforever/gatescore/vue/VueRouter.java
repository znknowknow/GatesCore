package org.javaforever.gatescore.vue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontManyToMany;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.StatementList;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.utils.StringUtil;

public class VueRouter implements Comparable<VueRouter>,Cloneable,Serializable{
	private static final long serialVersionUID = -2942380430997684889L;
	protected String standardName = "Pages";
	protected Set<FrontDomain> domains = new TreeSet<FrontDomain>();
	protected Set<FrontManyToMany> mtms = new TreeSet<FrontManyToMany>();
	@Override
	public int compareTo(VueRouter o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getFileName() {
		return StringUtil.lowerFirst(this.standardName)+".js";
	}
	public Set<FrontDomain> getDomains() {
		return domains;
	}
	public void setDomains(Set<FrontDomain> domains) {
		this.domains = domains;
	}
	public Set<FrontManyToMany> getMtms() {
		return mtms;
	}
	public void setMtms(Set<FrontManyToMany> mtms) {
		this.mtms = mtms;
	}
	public void addMtms(Set<FrontManyToMany> mtms) {
		this.mtms.addAll(mtms);
	}
	public StatementList generateStatementList() throws Exception{
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"/** When your routing table is too long, you can split it into small modules**/"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import Layout from '@/views/layout/Layout'"));
		sList.add(new Statement(4000L,0,""));
		sList.add(new Statement(5000L,0,"const pagesRouter = {"));
		sList.add(new Statement(6000L,0,"path: '',"));
		sList.add(new Statement(7000L,0,"component: Layout,"));
		sList.add(new Statement(8000L,0,"redirect: 'noredirect',"));
		sList.add(new Statement(9000L,0,"name: 'Pages',"));
		sList.add(new Statement(10000L,0,"meta: {"));
		sList.add(new Statement(11000L,1,"title: 'pages',"));
		sList.add(new Statement(12000L,1,"icon: 'component'"));
		sList.add(new Statement(13000L,0,"},"));
		sList.add(new Statement(14000L,0,"children: ["));
		
		sList.add(new Statement(15000L,1,"{"));
		sList.add(new Statement(16000L,1,"path: 'index',"));
		sList.add(new Statement(17000L,1,"component: () => import('@/views/pages/index'),"));
		sList.add(new Statement(18000L,1,"name: 'index',"));
		sList.add(new Statement(19000L,1,"meta: { title: 'Index', noCache: true }"));
		sList.add(new Statement(20000L,1,"},"));

		long serial = 21000L;
		for (FrontDomain d:this.domains) {
			sList.add(new Statement(serial,1,"{"));
			sList.add(new Statement(serial+1000L,1,"path: '"+d.getPlural().toLowerCase()+"',"));
			sList.add(new Statement(serial+2000L,1,"component: () => import('@/views/pages/"+d.getPlural().toLowerCase()+"'),"));
			sList.add(new Statement(serial+3000L,1,"name: '"+d.getPlural().toLowerCase()+"',"));
			sList.add(new Statement(serial+4000L,1,"meta: { title: '"+d.getCapFirstPlural()+"', noCache: true }"));
			sList.add(new Statement(serial+5000L,1,"},"));
			serial += 6000L;
		}
		for (FrontManyToMany mtm:this.mtms) {
			sList.add(new Statement(serial,1,"{"));
			sList.add(new Statement(serial+1000L,1,"path: '"+mtm.getStandardName().toLowerCase()+"',"));
			sList.add(new Statement(serial+2000L,1,"component: () => import('@/views/pages/"+mtm.getStandardName().toLowerCase()+"'),"));
			sList.add(new Statement(serial+3000L,1,"name: '"+mtm.getStandardName().toLowerCase()+"',"));
			sList.add(new Statement(serial+4000L,1,"meta: { title: '"+mtm.getStandardName()+"', noCache: true }"));
			sList.add(new Statement(serial+5000L,1,"},"));
			serial += 6000L;
		}

		sList.add(new Statement(serial,0,"]"));
		sList.add(new Statement(serial+1000L,0,"}"));
		sList.add(new Statement(serial+2000L,0,""));
		sList.add(new Statement(serial+3000L,0,"export default pagesRouter"));
		return WriteableUtil.merge(sList);
	}
}
