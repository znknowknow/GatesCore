package org.javaforever.gatescore.core;

import java.io.Serializable;


public class Statement implements Cloneable,Serializable,Writeable{
	private static final long serialVersionUID = 5475656647744069188L;
	protected String standardName;
	protected Long serial = 0L;
	protected String statement = "";
	protected Integer indent = 0;

	public Statement(long serial, String statement){
		super();
		this.serial = serial;
		this.indent = 0;
		this.statement = statement;
	}
	
	public Statement(long serial, int intent, String statement){
		super();
		this.serial = serial;
		this.statement = statement;
		this.indent = intent;
	}
	public Statement(){
		this(0,"");
	}
	
	@Override
	public Long getSerial() {
		return this.serial;
	}
	
	public void setSerial(Long serial){
		this.serial = serial;
	}

	@Override
	public String getContent() {
		StringBuilder sb = new StringBuilder();
		for (int i=0 ; i < this.indent; i++){
			sb.append("\t");
		}
		return sb.append(this.statement).toString();
	}

	@Override
	public String getStatement(long pos) {
		return this.statement;
	}
	 
	public void setStatement(String statement){
		this.statement = statement;
	}
	@Override
	public String getContentWithSerial() {
		StringBuilder sb = new StringBuilder();		
		sb.append(this.serial).append("\t\t：");
		for (int i=0 ; i < this.indent; i++) sb.append("\t");
		sb.append(this.statement);
		return sb.toString();
	}
	public int getIndent() {
		return indent;
	}
	public void setIndent(int indent) {
		this.indent = indent;
	}
	public String getStatement() {
		return statement;
	}

	@Override
	public String getStandardName() {
		return this.standardName;
	}

	@Override
	public int compareTo(Writeable o) {
		return this.getSerial().compareTo(o.getSerial());
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

}
