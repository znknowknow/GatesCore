package org.javaforever.gatescore.core;

import java.io.Serializable;

public class FrontType implements Serializable,Comparable<FrontType>,Cloneable{
	private static final long serialVersionUID = 7115120708596524490L;
	protected String typeName ="";
	protected FrontDomain domain;
	protected boolean templated = false;
	protected String packageToken ="";
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public FrontDomain getDomain() {
		return domain;
	}
	public void setDomain(FrontDomain domain) {
		this.domain = domain;
	}
	
	public String generateTypeString(){
		if (this.isTemplated())
			return (this.typeName + "<" + this.domain.getStandardName() + ">");
		else 
			return this.typeName;
	}
	public boolean isTemplated() {
		return templated;
	}
	public void setTemplated(boolean templated) {
		this.templated = templated;
	}
	public FrontType(){
		super();
	}
	public FrontType(String typeName, FrontDomain domain,String packageToken){
		super();
		this.typeName = typeName;
		if (domain != null){
			this.domain = domain;
			this.templated = true;
			this.packageToken = packageToken;
		}
	}
	
	public FrontType(String typeName,String packageToken){
		super();
		this.typeName = typeName;
		this.packageToken = packageToken;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	public FrontType(String typeName){
		super();
		this.typeName = typeName;
	}
	public boolean equals(Object o){
		FrontType rightType = (FrontType)o;
		if (this.typeName.equals(rightType.typeName) && this.domain.equals(rightType.getDomain()) && this.packageToken.equals(rightType.getPackageToken())){
			return true;
		}
		return false;
	}
	
	public String toString(){
		if (this.domain == null){
			return this.typeName;
		} else {
			if (this.typeName == null || "".equals(this.typeName)){
				return this.domain.getStandardName();
			}else {
				return this.typeName + "<" +this.domain.getStandardName() + ">";
			}
		}
	}
	
	public boolean isLong(){
		return this.typeName.equalsIgnoreCase("long");
	}
	
	public boolean isInt(){
		return this.typeName.equals("int") || this.typeName.equals("Integer");
	}
	
	public static FrontType getClassType(FrontType type){
		String typeStr = type.getTypeName();
		if ("int".equals(typeStr)) return new FrontType("Integer");
		else if ("long".equals(typeStr)) return new FrontType("Long");
		else if ("boolean".equals(typeStr)) return new FrontType("Boolean");
		else if ("float".equals(typeStr)) return new FrontType("Float");
		else if ("double".equals(typeStr)) return new FrontType("Double");
		else return type;
	}
	
	public static FrontType getClassType(String typeStr){
		if ("int".equals(typeStr)) return new FrontType("Integer");
		else if ("long".equals(typeStr)) return new FrontType("Long");
		else if ("boolean".equals(typeStr)) return new FrontType("Boolean");
		else if ("float".equals(typeStr)) return new FrontType("Float");
		else if ("double".equals(typeStr)) return new FrontType("Double");
		else return new FrontType(typeStr);
	}
	
	public static FrontType getOracleClassType(String typeStr){
		if ("int".equals(typeStr)) return new FrontType("Integer");
		else if ("long".equals(typeStr)) return new FrontType("String");
		else if ("boolean".equals(typeStr)) return new FrontType("Integer");
		else if ("float".equals(typeStr)) return new FrontType("Float");
		else if ("double".equals(typeStr)) return new FrontType("Double");
		else return new FrontType(typeStr);
	}
	
	public FrontType getClassType(){
		return FrontType.getClassType(this.typeName);
	}
	@Override
	public int compareTo(FrontType o) {
		return this.generateTypeString().compareTo(o.generateTypeString());
	}	
}
