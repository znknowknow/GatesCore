package org.javaforever.gatescore.core;

import java.io.Serializable;

public abstract class FrontUtil  implements Comparable<FrontUtil> ,Cloneable,Serializable {
	private static final long serialVersionUID = -5053690959124602670L;
	protected String standardName;
	protected String fileName;
	protected String content;
	protected String packageToken;
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	public abstract String generateUtilString();
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
