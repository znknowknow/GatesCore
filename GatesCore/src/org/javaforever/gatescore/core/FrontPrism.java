package org.javaforever.gatescore.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.javaforever.gatescore.complexverb.Assign;
import org.javaforever.gatescore.complexverb.FrontTwinsVerb;
import org.javaforever.gatescore.complexverb.ListMyActive;
import org.javaforever.gatescore.complexverb.ListMyAvailableActive;
import org.javaforever.gatescore.complexverb.Revoke;
import org.javaforever.gatescore.exception.ValidateException;
import org.javaforever.gatescore.utils.StringUtil;
import org.javaforever.gatescore.verb.Activate;
import org.javaforever.gatescore.verb.ActivateAll;
import org.javaforever.gatescore.verb.Add;
import org.javaforever.gatescore.verb.Clone;
import org.javaforever.gatescore.verb.CloneAll;
import org.javaforever.gatescore.verb.Delete;
import org.javaforever.gatescore.verb.DeleteAll;
import org.javaforever.gatescore.verb.Export;
import org.javaforever.gatescore.verb.FindById;
import org.javaforever.gatescore.verb.FindByName;
import org.javaforever.gatescore.verb.ListActive;
import org.javaforever.gatescore.verb.ListAll;
import org.javaforever.gatescore.verb.ListAllByPage;
import org.javaforever.gatescore.verb.SearchByFieldsByPage;
import org.javaforever.gatescore.verb.SearchByName;
import org.javaforever.gatescore.verb.SoftDelete;
import org.javaforever.gatescore.verb.SoftDeleteAll;
import org.javaforever.gatescore.verb.Toggle;
import org.javaforever.gatescore.verb.ToggleOne;
import org.javaforever.gatescore.verb.Update;
import org.javaforever.gatescore.vue.ElementUIGridPage;
import org.javaforever.gatescore.vue.ElementUIMtmPage;
import org.javaforever.gatescore.vue.VueAPIJs;

public class FrontPrism  implements Comparable<FrontPrism>,Cloneable,Serializable {
	private static final long serialVersionUID = 3017829512307723077L;
	protected static Logger logger = Logger.getLogger(FrontPrism.class);
	protected String standardName;
	protected FrontDomain domain;
	protected String prismComment;
	protected List<FrontUtil> utils = new ArrayList<FrontUtil>();
	protected List<FrontController> controllers = new ArrayList<FrontController>();
	protected String folderPath = "D:/JerryWork/Infinity/testFiles/";
	protected String packageToken;
	protected Set<FrontVerb> verbs = new TreeSet<FrontVerb>();
	protected Set<FrontTwinsVerb> twinsverbs = new TreeSet<FrontTwinsVerb>();
	protected String label;
	protected Set<Pair> manyToManySlaveNames = new TreeSet<Pair>();
	protected Set<FrontManyToMany> manyToManies = new TreeSet<FrontManyToMany>();
	protected Set<FrontDomain> projectDomains = new TreeSet<FrontDomain>();
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	
	public void generatePrismFromDomain() throws ValidateException, Exception {
		if (this.domain != null) {
			if (this.getPackageToken() != null) {
				this.domain.setPackageToken(packageToken);
			}

			FrontVerb listAll = new ListAll(this.domain);
			FrontVerb update = new Update(this.domain);
			FrontVerb delete = new Delete(this.domain);
			FrontVerb add = new Add(this.domain);
			FrontVerb softdelete = new SoftDelete(this.domain);
			FrontVerb findbyid = new FindById(this.domain);
			FrontVerb findbyname = new FindByName(this.domain);
			FrontVerb searchbyname = new SearchByName(this.domain);
			FrontVerb listactive = new ListActive(this.domain);
			FrontVerb listAllByPage = new ListAllByPage(this.domain);
			FrontVerb deleteAll = new DeleteAll(this.domain);
			FrontVerb softDeleteAll = new SoftDeleteAll(this.domain);
			FrontVerb toggle = new Toggle(this.domain);
			FrontVerb toggleOne = new ToggleOne(this.domain);
			FrontVerb searchByFieldsByPage = new SearchByFieldsByPage(this.domain);
			FrontVerb activate = new Activate(this.domain);
			FrontVerb activateAll = new ActivateAll(this.domain);
			FrontVerb export = new Export(this.domain);
			FrontVerb clone = new Clone(this.domain);
			FrontVerb cloneAll = new CloneAll(this.domain);

			this.addVerb(listAll);
			this.addVerb(update);
			this.addVerb(delete);
			this.addVerb(add);
			this.addVerb(softdelete);
			this.addVerb(findbyid);
			this.addVerb(findbyname);
			this.addVerb(searchbyname);
			this.addVerb(listactive);
			this.addVerb(listAllByPage);
			this.addVerb(deleteAll);
			this.addVerb(softDeleteAll);
			this.addVerb(toggle);
			this.addVerb(toggleOne);
			this.addVerb(searchByFieldsByPage);
			this.addVerb(activate);
			this.addVerb(activateAll);
			this.addVerb(export);
			this.addVerb(clone);
			this.addVerb(cloneAll);
			
			if (this.domain.manyToManies != null && this.domain.manyToManies.size() > 0) {
				for (FrontManyToMany mtm : this.domain.manyToManies) {
					String slaveName = mtm.getManyToManySalveName();
					String masterName = this.domain.getStandardName();
					if (setContainsDomain(this.projectDomains, masterName)
							&& setContainsDomain(this.projectDomains, slaveName)) {
						FrontDomain myslave = (FrontDomain)lookupDoaminInSet(this.projectDomains, slaveName).clone();
						if (!StringUtil.isBlank(mtm.getSlaveAlias())){
							myslave.setAlias(mtm.getSlaveAlias());
							myslave.setAliasLabel(mtm.getSlaveAliasLabel());
						}
						FrontManyToMany mymtm = new FrontManyToMany(lookupDoaminInSet(this.projectDomains, masterName),
								myslave,mtm.getMasterValue(),mtm.getValues());	
						mymtm.setSlaveAlias(myslave.getAlias());
						mymtm.setSlaveAliasLabel(mtm.getSlaveAliasLabel());
						mymtm.setStandardName("Link"+StringUtil.capFirst(masterName)+StringUtil.capFirst(myslave.getAliasOrName()));
						mymtm.setValues(mtm.getValues());
						this.manyToManies.add(mymtm);
						Assign assign = new Assign(mymtm);
						Revoke revoke = new Revoke(mymtm);
						ListMyActive lma = new ListMyActive(mymtm);
						ListMyAvailableActive lmaa = new ListMyAvailableActive(mymtm);
						addTwinsVerb(assign);
						addTwinsVerb(revoke);
						addTwinsVerb(lma);
						addTwinsVerb(lmaa);						
						logger.debug("JerryDebug:Master:"+masterName+":"+myslave.getAlias()+"");
					} else {
						ValidateInfo validateInfo = new ValidateInfo();
						validateInfo.addCompileError("棱柱" + this.getText() + "多对多设置有误。");
						ValidateException em = new ValidateException(validateInfo);
						throw em;
					}
				}
			}
		}
	}
	
	public String getText() {
		if (this.label != null && !this.label.equals(""))
			return this.label;
		else
			return this.standardName;
	}

	protected Boolean setContainsDomain(Set<FrontDomain> set, String domainName) {
		for (FrontDomain d : set) {
			if (d.getStandardName().equals(domainName))
				return true;
		}
		return false;
	}

	
	protected FrontDomain lookupDoaminInSet(Set<FrontDomain> set, String domainName) {
		for (FrontDomain d : set) {
			if (d.getStandardName().equals(domainName))
				return d;
		}
		return null;
	}
	
	public ValidateInfo validate() {
		ValidateInfo info = new ValidateInfo();
		info.setSuccess(true);
		return info;
	}
	
	public  void writeToFile(String filePath, String content) throws Exception{
		File f = new File(filePath);
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		f.createNewFile();
		try (Writer fw = new BufferedWriter( new OutputStreamWriter(new FileOutputStream(f.getAbsolutePath()),"UTF-8"))){			
	        fw.write(content,0,content.length());
		}
	}
	
	public void generatePrismFiles() throws ValidateException {
		ValidateInfo info = this.validate();
		if (info.success() == false) {
			ValidateException e = new ValidateException(info);
			throw e;
		}
		try {
			ElementUIGridPage etp = new ElementUIGridPage(this.domain);
			etp.setVerbs(this.verbs);
			etp.setTwinsverbs(this.twinsverbs);
			writeToFile(folderPath + "src/views/pages/" + this.domain.getPlural().toLowerCase() + ".vue",
					etp.generateStatementList().getContent());
			
			VueAPIJs vapi = new VueAPIJs(this.domain);
			vapi.setTwinsverbs(this.twinsverbs);
			writeToFile(folderPath + "src/api/" + vapi.getFileName(),
					vapi.generateStatementList().getContent());
				
			for (FrontManyToMany mtm : this.manyToManies) {
				if (!mtm.getMaster().isLegacy()&&!mtm.getSlave().isLegacy()) { 
					ElementUIMtmPage mtmp = new ElementUIMtmPage(mtm);
					writeToFile(folderPath + "src/views/pages/" + mtm.getStandardName().toLowerCase() + ".vue",
							mtmp.generateStatementList().getContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int compareTo(FrontPrism o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public String getPrismComment() {
		return prismComment;
	}
	public void setPrismComment(String prismComment) {
		this.prismComment = prismComment;
	}
	public List<FrontUtil> getUtils() {
		return utils;
	}
	public void setUtils(List<FrontUtil> utils) {
		this.utils = utils;
	}
	public List<FrontController> getControllers() {
		return controllers;
	}
	public void setControllers(List<FrontController> controllers) {
		this.controllers = controllers;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public String getPackageToken() {
		return packageToken;
	}
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Set<Pair> getManyToManySlaveNames() {
		return manyToManySlaveNames;
	}
	public void setManyToManySlaveNames(Set<Pair> manyToManySlaveNames) {
		this.manyToManySlaveNames = manyToManySlaveNames;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public String getFooter() {
		return footer;
	}
	public void setFooter(String footer) {
		this.footer = footer;
	}
	public Set<FrontManyToMany> getManyToManies() {
		return manyToManies;
	}
	public void setManyToManies(Set<FrontManyToMany> manyToManies) {
		this.manyToManies = manyToManies;
	}
	public Set<FrontDomain> getProjectDomains() {
		return projectDomains;
	}
	public void setProjectDomains(Set<FrontDomain> projectDomains) {
		this.projectDomains = projectDomains;
	}
	public FrontDomain getDomain() {
		return domain;
	}
	public void setDomain(FrontDomain domain) {
		this.domain = domain;
	}
	public Set<FrontVerb> getVerbs() {
		return verbs;
	}
	public void setVerbs(Set<FrontVerb> verbs) {
		this.verbs = verbs;
	}
	public void addVerb(FrontVerb verb) {
		this.verbs.add(verb);
	}

	public Set<FrontTwinsVerb> getTwinsverbs() {
		return twinsverbs;
	}

	public void setTwinsverbs(Set<FrontTwinsVerb> twinsverbs) {
		this.twinsverbs = twinsverbs;
	}
	
	public void addTwinsVerb(FrontTwinsVerb twinsVerb) {
		this.twinsverbs.add(twinsVerb);
	}
}
