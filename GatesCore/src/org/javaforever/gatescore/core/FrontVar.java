package org.javaforever.gatescore.core;

import java.io.Serializable;

public class FrontVar  implements Comparable<FrontVar>,Cloneable,Serializable {
	private static final long serialVersionUID = 2297306404752425206L;
	protected String varName;
	protected String varAlias;
	protected String value;
	
	public FrontVar(String varName) {
		super();
		this.varName = varName;
	}
	@Override
	public int compareTo(FrontVar o) {
		return this.varName.compareTo(o.getVarName());
	}
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public String getVarAlias() {
		return varAlias;
	}
	public void setVarAlias(String varAlias) {
		this.varAlias = varAlias;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
