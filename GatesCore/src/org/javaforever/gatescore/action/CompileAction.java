package org.javaforever.gatescore.action;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.log4j.Logger;
import org.javaforever.gatescore.compiler.SGSCompiler;
import org.javaforever.gatescore.core.FrontProject;
import org.javaforever.gatescore.core.ValidateInfo;
import org.javaforever.gatescore.exception.ValidateException;

public class CompileAction {
	protected static Logger logger = Logger.getLogger(CompileAction.class);
	public static String compile(String sgscode,String templatesPath,String savePath,boolean showBackendProject,boolean ignoreWarning, boolean useController) throws Exception{
		if (!ignoreWarning) {
			return compilePack(sgscode,templatesPath,savePath,showBackendProject,useController);
		}else {
			return ignoreWarningAndCompilePack(sgscode,templatesPath,savePath,showBackendProject,useController);
		}
	}
	public static String compile(String sgscode,String templatesPath,String savePath,boolean showBackendProject,boolean ignoreWarning) throws Exception{
		if (!ignoreWarning) {
			return compilePack(sgscode,templatesPath,savePath,showBackendProject,false);
		}else {
			return ignoreWarningAndCompilePack(sgscode,templatesPath,savePath,showBackendProject,false);
		}
	}
	
	public static String compilePackFile(String sgsFilePath,String templatesPath,String savePath,boolean showBackendProject) throws Exception {
		return compilePackFile(sgsFilePath,templatesPath,savePath,showBackendProject,false);
	}
	
	public static String compilePackFile(String sgsFilePath,String templatesPath,String savePath,boolean showBackendProject,boolean useController) throws Exception {
		List<String> sgsLines = Files.readAllLines(Paths.get(sgsFilePath),StandardCharsets.UTF_8);
		StringBuilder sgses = new StringBuilder("");
		for (String s : sgsLines) {
			sgses.append(s).append("\n");
		}
		return compilePack(sgses.toString(),templatesPath,savePath,showBackendProject,useController);
	}
	
	public static String ignoreWarningAndCompilePackile(String sgsFilePath,String templatesPath,String savePath,boolean showBackendProject) throws Exception {
		return ignoreWarningAndCompilePackile(sgsFilePath,templatesPath,savePath,showBackendProject,false);
	}
	
	public static String ignoreWarningAndCompilePackile(String sgsFilePath,String templatesPath,String savePath,boolean showBackendProject,boolean useController)  throws Exception {
		List<String> sgsLines = Files.readAllLines(Paths.get(sgsFilePath),StandardCharsets.UTF_8);
		StringBuilder sgses = new StringBuilder("");
		for (String s : sgsLines) {
			sgses.append(s).append("\n");
		}
		return ignoreWarningAndCompilePack(sgses.toString(),templatesPath,savePath,showBackendProject,useController);
	}

	public static String compilePack(String sgscode,String templatesPath,String savePath,boolean showBackendProject) throws Exception {
		return compilePack(sgscode,templatesPath,savePath,showBackendProject,false);
	}	
		
	public static String compilePack(String sgscode,String templatesPath,String savePath,boolean showBackendProject,boolean useController) throws Exception {
		try {
			logger.debug(sgscode);
			FrontProject project = SGSCompiler.translate(sgscode,false);
			project.setBackendProjectName(project.getStandardName());
			project.setStandardName(project.getStandardName()+"FrontEnd");		
			project.setShowBackendProject(showBackendProject);
			project.setTemplatesPath(templatesPath);
			project.setFolderPath(savePath);
			project.setUseController(useController);
			project.generateProjectZip();

			return project.getStandardName();
		}catch(Exception e){
			if (!(e instanceof ValidateException)) {
				e.printStackTrace();
				Exception ex= new Exception(e.getMessage());
				throw ex;
			}else{
				throw e;
			}
		}
	}
	
	public static String ignoreWarningAndCompilePack(String sgscode,String templatesPath,String savePath,boolean showBackendProject) throws Exception {
		return ignoreWarningAndCompilePack(sgscode,templatesPath,savePath,showBackendProject,false);
	}
	
	public static String ignoreWarningAndCompilePack(String sgscode,String templatesPath,String savePath,boolean showBackendProject,boolean useController) throws Exception {
		try {
			logger.debug(sgscode);
			FrontProject project = SGSCompiler.translate(sgscode,true);
			project.setBackendProjectName(project.getStandardName());
			project.setStandardName(project.getStandardName()+"FrontEnd");		
			project.setShowBackendProject(showBackendProject);
			project.setTemplatesPath(templatesPath);
			project.setFolderPath(savePath);
			project.setUseController(useController);
			
			project.generateProjectZip();

			return project.getStandardName();
		} catch (ValidateException ev){
			ValidateInfo info = ev.getValidateInfo();
			List<String> compileErrors = info.getCompileErrors();
			List<String> compileWarnings = info.getCompileWarnings();
			if (compileErrors.size()==0) return "success";
			else throw ev;
		}catch(Exception e){
			if (!(e instanceof ValidateException)) {
				ValidateException ex= new ValidateException(e.getMessage());
				throw ex;
			}else{
				throw e;
			}
		}
	}
	
	public static void main(String [] args) throws Exception{
		String sgsFolderPath = "C:/Users/jerry.shen03/git/GatesCore/GatesCore/src/org/javaforever/gatescore/action/";
		String sgsFileName = "CompleteSample.sgs";
		String templatesPath = "C:/Users/jerry.shen03/git/GatesCore/GatesCore/templates/";
		String savePath = "D:/JerryWork/CodeGenerator/TestBed/";
		compilePackFile(sgsFolderPath+sgsFileName,templatesPath,savePath,true,false);
	}
}
